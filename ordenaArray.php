<?php

  $v[0]=array("nombre"=>"Juan", "apellido"=>"Perez","edad"=>26);
  $v[1]=array("nombre"=>"Carlos", "apellido"=>"Gomez","edad"=>30);
  $v[2]=array("nombre"=>"Ernesto", "apellido"=>"Ramirez","edad"=>22);

  function orderarray($array, $dimension){
    $length = count($array);
    for($outer = 0; $outer < $length; $outer++){
      for($inner = 0; $inner < $length; $inner++){
        if($array[$outer][$dimension]<$array[$inner][$dimension]){
          $temp = $array[$outer];
          $array[$outer]=$array[$inner];
          $array[$inner]=$temp;
        }

      }
    }
    return $array;
  }

  print_r(orderArray($v,'edad'));

?>
